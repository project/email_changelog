<?php

/**
 * @file
 * User page callbacks for the email_changelog module.
 */

/**
 * Display tab page from menu callback.
 *
 * @param $account
 *   User object.
 */
function email_changelog_history($account) {
  return email_changelog_report($account);
}

function email_changelog_report($account = NULL) {
  $output = '';
  $rows = [];

  $header = array(
    array(
      'data' => t('Date'),
      'style' => 'width: 20%;',
    ),
    array(
      'data' => t('Old Email'),
      'style' => 'width: 20%;',
    ),
    array(
      'data' => t('New Email'),
      'style' => 'width: 20%;',
    ),
    array(
      'data' => t('User'),
      'style' => 'width: 10%;',
    ),
    array(
      'data' => t('Modified by'),
      'style' => 'width: 10%;',
    ),
    array(
      'data' => t('Additional Info'),
      'style' => 'width: 20%;',
    ),
  );

  $query = db_select('email_changelog', 'ec');
  if ($account)
    $query->innerJoin('users', 'u1', 'ec.uid = u1.uid');
  else
    $query->leftJoin('users', 'u1', 'ec.uid = u1.uid');
  $query->leftJoin('users', 'u2', 'ec.logged_in_uid = u2.uid');
  $query->extend('PagerDefault')
    ->limit(50)
    ->fields('ec')
    ->fields('u1', array('name'))
    ->fields('u2', array('name'))
    ->orderBy('ec.stamp', 'DESC');
  if ($account) {
    $query->condition('ec.uid', $account->uid);
  }
  $result = $query->execute()
    ->fetchAllAssoc('id');
  foreach ($result as $row) {
    $rows[] = [
      format_date($row->stamp),
      $row->old_email_address,
      $row->new_email_address,
      l($row->name, 'user/' . $row->uid, ['attributes' => ['target' => '_blank']]),
      l($row->u2_name, 'user/' . $row->logged_in_uid, ['attributes' => ['target' => '_blank']]),
      implode('<br/>', (array)json_decode($row->data)),
    ];
  }

  if (sizeof($rows)) {
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('style' => 'width: 99%;')));
    $output .= theme('pager', array('tags' => NULL));
  }
  return $output ? $output : t('No Email Change Log history found.');
}
