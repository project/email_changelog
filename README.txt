
Description
---------------
Email Changelog automatically logs all email changes made.
A record of these changes is shown in a Email change tab on each user's page.
Admin can also see all changelogs in the Reports part just like watchdog.

This module can be useful when there are multiple administrators for a site,
and you need auditing of manual email changes.


Dependencies
---------------
None.


Usage
---------------
Email Changelog will automatically start recording all email changes. No further
configuration is necessary for this functionality, the module will do this "out
of the box". A record of these changes is shown in a Email Changelog tab on
each user's page. Users will need either "View email changelog" or
"View own email changelog" access permissions to view the tab.


Author
---------------
Sadashiv Dalvi

Module development originally sponsored by Gokhale Method Enterprise,
https://gokhalemethod.com
